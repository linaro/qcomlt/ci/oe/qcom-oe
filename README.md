Build Yocto images for Qualcomm Landing Team

- [![kirkstone status](../badges/kirkstone/pipeline.svg?key_text=kirkstone)](../pipelines/?ref=kirkstone)
  [qcom-armv8a](https://snapshots.linaro.org/member-builds/qcomlt/boards/qcom-armv8a/openembedded/kirkstone/)
  [qcom-armv7a](https://snapshots.linaro.org/member-builds/qcomlt/boards/qcom-armv7a/openembedded/kirkstone/)
- [![scarthgap status](../badges/scarthgap/pipeline.svg?key_text=scarthgap)](../pipelines/?ref=scarthgap)
  [qcom-armv8a](https://snapshots.linaro.org/member-builds/qcomlt/boards/qcom-armv8a/openembedded/scarthgap/)
  [qcom-armv7a](https://snapshots.linaro.org/member-builds/qcomlt/boards/qcom-armv7a/openembedded/scarthgap/)
- [![styhead status](../badges/styhead/pipeline.svg?key_text=styhead)](../pipelines/?ref=styhead)
  [qcom-armv8a](https://snapshots.linaro.org/member-builds/qcomlt/boards/qcom-armv8a/openembedded/styhead/)
  [qcom-armv7a](https://snapshots.linaro.org/member-builds/qcomlt/boards/qcom-armv7a/openembedded/styhead/)
- [![master status](../badges/master/pipeline.svg?key_text=master)](../pipelines/?ref=master)
  [qcom-armv8a](https://snapshots.linaro.org/member-builds/qcomlt/boards/qcom-armv8a/openembedded/master/)
  [qcom-armv7a](https://snapshots.linaro.org/member-builds/qcomlt/boards/qcom-armv7a/openembedded/master/)

Don't forget to update .gitlab-ci.yml when adding new branches
